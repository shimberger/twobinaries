package main

import "fmt"
import "bitbucket.org/shimberger/twobinaries"
import "bitbucket.org/shimberger/twobinaries/utils"

func main() {
	fmt.Printf("Hello this is the server: '%v' '%v'\n", twobinaries.Common(), utils.Util())
}
